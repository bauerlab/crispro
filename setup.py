#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 23 13:14:06 2017

Setup script for CRISPRO
@author: mitchel cole
"""

from setuptools import setup
import re
from os import path

def main():
    version = re.search('^__version__\s*=\s*"(.*)"', open('bin/crispro').read(), re.M).group(1)
    cur_dir = path.abspath(path.dirname(__file__))
    with open(path.join(cur_dir, 'README'), encoding='utf-8') as f:
        long_description = f.read()
        
    setup(name='crispro',
          version=version,
          author='Mitchel Cole, Vivien Schoonenberg',
          author_email='placeholder',
          url='https://gitlab.com/bauerlab/saturating_mutagenesis_pipeline',
          description='Analysis and visualization of CRISPR Saturating Mutagenesis Screens',
          long_description=long_description,
          include_package_data=True,
          packages=['crispro'],
          package_dir={'crispro':'crispro'},
          package_data={'crispro':['annotations/*','rscripts/*.R']},
          scripts=['bin/crispro'],
          classifiers=[
                  'Development Status :: 3 - Alpha',
                  'Environment :: Console',
                  'Intended Audience :: Science/Research',
                  'Operating System :: POSIX',
                  'Topic :: Scientific/Engineering :: Bio-Informatics',
                  'Programming Language :: Python',
                  'Programming Language :: R'
                  ],

          install_requires=[
                  'numpy',
                  'pandas',
                  'matplotlib',
                  'biopython',
                  'argparse',
    			  'seaborn',
                  'cutadapt',
                  'pymol'
                  ]
          )
if __name__ == '__main__':
    main()
