# -*- coding: utf-8 -*-
'''
August 2017
Module for crispro, a script to process sequencing data from dense CRISPR tiling screens.
'''

import os, re, itertools, urllib.request, urllib.parse, urllib.error, logging, csv, math, pickle
from math import cos, sin, pi, sqrt, acos, asin, atan2
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import pandas as pd
import numpy as np
from Bio import pairwise2
from Bio.Align import substitution_matrices
from Bio.PDB import *
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.Polypeptide import PPBuilder
from scipy import stats
import pymol
#pymol.finish_launching(['pymol', '-cq'])

CRISPRO_DIR = os.path.dirname(__file__)

def download_PDBs(list_pdbs, main_output_directory):
    url = 'https://files.rcsb.org/download/'
    #download each PDB in list
    for pdb_name in list_pdbs:
        path = os.path.join(main_output_directory,pdb_name)
        #sort pdbs into own subdirectry. check if directory already exist, otherwise make it, change to the directory to save structures.
        # if the dir already exists, the pdb is already downloaded, no need to do again.
        if not os.path.exists(path):
            os.mkdir(path)
        filename = os.path.join(path, pdb_name.lower()+'.pdb')
        url_complete = url + pdb_name + '.pdb'
        if not os.path.exists(filename):
            urllib.request.urlretrieve(url_complete, filename)

def save_sequence(pdb):
    parser = PDBParser()
    ppb = PPBuilder()
    df_seq = pd.DataFrame()
    pdb_file = pdb.split('/')[-1]
    pdb_id = pdb_file.split('.')[0]
    structure = parser.get_structure(pdb_id, pdb)
    for model in structure:
        for chain in model:
            seq=""
            for pp in ppb.build_peptides(chain):
                seq = seq + str(pp.get_sequence())
            seq_dict = {'chain':chain.id, 'sequence':seq}
            df_new_row = pd.DataFrame.from_dict([seq_dict])
            df_seq = pd.concat([df_seq,df_new_row])
    df_seq['PDB_id'] = pdb_id
    df_seq.reset_index(drop=True)
    return df_seq

def list2conversion_file (list_protein, list_pdb, output_file):
    # create indexing for each of the proteins and a seperate index which will represent the new amino acid numbering of the second protein.
    index_protein = 1
    index_pdb = 0
    new_AA_numbering_pdb = 0
    old_numbering_pdb = 1
    # create outputfile   writer = csv.writer(f)
    outfile = open(output_file, 'w')
    writer = csv.writer(outfile, delimiter= ',')
    writer.writerow( ('Old.AA.pos.PDB', 'AA.PDB', 'New.AA.pos.PDB', 'AA.complete.prot', 'Pos.AA.complete.prot') )
    # loop through every amino acid in protein 1
    for AA_protein in list_protein:
        # whenever there is no gap in either one of the proteins, the amino acids are aligned in this position, so the numbering will continue accordingly (+1).
        if not list_pdb[index_pdb] == '-' and AA_protein != '-':
            new_AA_numbering_pdb = new_AA_numbering_pdb + 1
            writer.writerow( (old_numbering_pdb, list_pdb[index_pdb], new_AA_numbering_pdb, AA_protein, index_protein) )
    # if there is a gap in alignment in protein 2, the counter needs to continue, but not do anything else (showing amino acids etc)
        elif list_pdb[index_pdb] == '-':
            new_AA_numbering_pdb = new_AA_numbering_pdb + 1
            old_numbering_pdb = old_numbering_pdb - 1
    #if there is a gap in alignment in protein 1, the counter should not continue, and for that particular amino acid in protein no new available position will be available.
        elif AA_protein == '-':
            index_protein = index_protein - 1
            writer.writerow( (old_numbering_pdb, list_pdb[index_pdb], 'N/A', AA_protein, '-') )
        index_pdb = index_pdb+1
        index_protein = index_protein+1
        old_numbering_pdb = old_numbering_pdb + 1
    outfile.close()

def align(protein_seq, pdb_seq, outfile):
    sequence1 = protein_seq
    sequence2 = pdb_seq
    blosum62 = substitution_matrices.load("BLOSUM62")
    local_alignments = pairwise2.align.localds(sequence1, sequence2, blosum62, -10, -0.5, one_alignment_only=True)
    local_compl_prot = local_alignments[0][0]
    local_pdb = local_alignments[0][1]
    list_local_compl_prot = list(local_compl_prot)
    list_local_pdb = list(local_pdb)
    list2conversion_file(list_local_compl_prot, list_local_pdb, outfile)

#need gene info to match PDB's to several isoforms and potential genes which didn't come up in the BLAST: Homo_sapiens_gene_synonyms_protID.csv
def align_specific_all(alignment_dir, df_prot, df_pdb_seqs, crispro_enspids, df_blast, pdb_id):
    #get the header from the pdb, and parse the source
    df_temp = df_blast[df_blast.PDB == pdb_id.upper()]
    df_temp.reset_index(drop=True,inplace=True)
    for index, row in df_temp.iterrows():
        chain = row['chain']
        peptide = row['ensembl_peptide_id']
        if peptide in crispro_enspids:
            df_seq_temp = df_prot[df_prot['ENSPID'] == peptide]
            df_seq_temp.reset_index(drop=True,inplace=True)
            complete_seq = df_seq_temp.loc[0,'Seq']
            df_pdb_seq_temp = df_pdb_seqs[(df_pdb_seqs['PDB_id']==pdb_id.lower()) & (df_pdb_seqs['chain']== chain)]
            if not df_pdb_seq_temp.empty:
                df_pdb_seq_temp.reset_index(drop=True,inplace=True)
                pdb_seq = df_pdb_seq_temp.loc[0,'sequence']
                outfile = os.path.join(alignment_dir,'local_coordinates_alignment_'+peptide+'_'+pdb_id+'_chain'+chain+'.csv')
                align(complete_seq, pdb_seq, outfile)

def write_b_factor_file(inputfile_coordinates, outdir, score_type, df_crispro):
    #load csv inputfile as pandas dataframe
    df_coordinates = pd.read_csv(inputfile_coordinates)
    #define gene, pdb and selection names from filename
    inputfile = inputfile_coordinates.split('/')[-1]
    split_file = inputfile.split('_', 5)
    #gene_name is ENSPID
    ENSPID = split_file[3]
    pdb = split_file[4]
    chain = split_file[5][5]
    df_data = df_crispro[df_crispro.ensembl_peptide_id==ENSPID].reset_index(drop=True)
    if not df_data.empty:
        outdir = os.path.join(outdir, chain)
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        #just write file for ONE score type at the time
        outfile = open(os.path.join(outdir, pdb+'_'+score_type+'_'+chain+'_'+ENSPID+'_Bfact.txt'),'w')
        for idx, row in df_coordinates.iterrows():
            if math.isnan(row['New.AA.pos.PDB']):
                outfile.write('-10\n')
            else:
                pos = row['New.AA.pos.PDB']
                if math.isnan(df_data[df_data['position']==pos][score_type]):
                    outfile.write('-10\n')
                else:
                    value = str(df_data[df_data['position']==pos].reset_index(drop=True).iloc[0][score_type]) +'\n'
                    outfile.write(value)
        outfile.close()
#import pymol
#pymol.finish_launching(['pymol', '-cq'])

def alphaToAll(sel, col="b",forceRebuild=False):
    """
    alphaToAll -- expand any property of the alpha carbons to all atoms in the residue

    PARAMS
        sel
            The selection or object (include "*" for all objects) to operate on.  This will
            read the alpha carbon's "COL" property and expand that down to all atoms in the
            given residues in sel.

        col
            Any valid PyMOL property.  For example, 'b' or 'q' or 'color', etc.
            DEFAULT: b, for B-factor as we most often overwrite that column

        forceRebuild
            If a color changes, this will rebuild the objects for us.  For speed, we
            DEFAULT this to False.

    RETURNS
        None, just epxands the properties as dicsussed above.

    NOTES
        This script is useful for instances where we update the b-factor column for the
        alpha carbons and then want to expand that color to all atoms for smoother looking
        images.

    AUTHOR:
        Jason Vertrees, 2009.
    """
    col = '(' + ','.join(col.split()) + ')'
    space = {'props': dict()}
    pymol.cmd.iterate('byca ({})'.format(sel), 'props[model,segi,chain,resi] = ' + col, space=space)
    pymol.cmd.alter(sel, col + ' = props.get((model,segi,chain,resi), ' + col + ')', space=space)
    if forceRebuild != False:
        pymol.cmd.rebuild(sel)

def load_newB (input_file, chain_selection="all"):
    input_file = "./" + str(input_file)
    #define selection, open inputfile, creat empty list to hold B-factors.
    selection = str(chain_selection) + '& !hetatm'
    #inFile = open(input_file, 'r')
    pymol.stored.new_b_list = []
    input = np.loadtxt(input_file)
    #loop through file, add each B-factor to list
    #for line in inFile.readlines():
    for item in input:
        pymol.stored.new_b_list.append(float(item))
    #inFile.close()
    #alter B-factor of each carbon atom in defined selection.
    pymol.cmd.alter(selection +" and n. CA","b =stored.new_b_list.pop(0)")

# Define a Python subroutine to colour atoms by B-factor, using predefined intervals
def color_consurf_adjusted(min=0, max=0, selection="all", amount_bins="9"):
    '''
    Defining and assiging CRISPRO colors, recoloring based on predefined bins.

    CREDITS: Script based on an original script by ConSurf, see http://consurf.tau.ac.il/

    Landau M., Mayrose I., Rosenberg Y., Glaser F., Martz E., Pupko T. and Ben-Tal N. 2005.
    ConSurf 2005: the projection of evolutionary conservation scores of residues on protein structures.
    Nucl. Acids Res. 33:W299-W302

    AUTHOR:
    Vivien A. C. Schoonenberg, 2017
     '''
    min= float(min)
    max = float(max)
    #these are default values, if the min and max values are given, they will not be changed.
    #if (min == 0 and max == 0 and amount_bins=="17"):
    #    min=-2.5
    #    max=2.5
    #if (min == 0 and max == 0 and amount_bins=="9"):
    #    min=-2.5
    #    max=2.5
    #print ("minimum=", min)
    #print ("maximum=", max)
    #color complete protein yellow
    color1 = [255,237,160]
    color_name1 = "color_no_data"
    pymol.cmd.set_color(color_name1, color1 )
    pymol.cmd.color(color_name1, "all")
    #define amount of colors = amount of bins, define RGB color code for each bin (lowest to highest)
    n_colours = int(amount_bins)
    #print (n_colours)
    if (amount_bins == "9"):
        colours = [
        [4,90,141],
        [43,140,190],
        [116,169,207],
        [189,201,225],
        [255,247,243],
        [251,180,185],
        [247,104,161],
        [197,27,138],
        [122,1,119]]
    elif (amount_bins == "17"):
        colours = [
        [2,56,88],
        [4,90,141],
        [5,112,176],
        [54,144,192],
        [116,169,207],
        [166,189,219],
        [208,209,230],
        [236,231,242],
        [255,247,243],
        [253,224,221],
        [252,197,192],
        [250,159,181],
        [247,104,161],
        [221,52,151],
        [174,1,126],
        [122,1,119],
        [73,0,106]]
    else:
        logging.error("Error - amount_bins = unknown, pick amount_bins=9 or amount_bins=17")
    #calculate bin size
    bin_size = ((max - min)) // n_colours
    #if not (amount_bins == '9' or '17'):
    #    bin_size = ((max - min) + 1) // n_colours
    #else:
    #    bin_size = 1
    # Loop through colour intervals
    for i in range(n_colours):
        lower = min + i * bin_size
        upper = lower + bin_size
        colour = colours[i]
        #  out B-factor limits and the RGB-colour for this group
        #print ("group ", (i+1))
        #print (lower, " - ", upper, " = ", colour)
        sel_strip = selection.replace(" ", "")
        # Define a unique name for the atoms which fall into this group
        group = sel_strip + "_group_" + str(i+1)
        # Compose a selection command which will select all atoms which are
        #    a) in the original selection, AND
        #    b) have B factor in range lower <= b < upper
        # lowest bin from -inf to upper bound and highest bin from lower bound to +inf (so all available data is included)
        sel_string = selection + " & ! b < " + str(lower)
        if (i==0):
            sel_string = selection + " & b < " + str(upper)
        elif (i==n_colours-1):
            sel_string = selection + " & b > " + str(lower)
        elif(i < n_colours - 1):
            sel_string += " & b < " + str(upper)
        else:
            sel_string += " & ! b > " + str(upper)
        # Select the atoms
        pymol.cmd.select(group, sel_string)
        # Create a new colour
        colour_name = "colour_" + str(i+1)
        pymol.cmd.set_color(colour_name, colour)
        # Colour the selected atoms
        pymol.cmd.color(colour_name, group)
    #Create new colour for insufficient sequences
    insuf_colour = [255,237,160]
    pymol.cmd.set_color("insufficient_colour", insuf_colour)
    #Colour atoms with B-factor of 10 using the new colour
    pymol.cmd.select("insufficient", selection + " & b = -10")
    pymol.cmd.color("insufficient_colour", "insufficient")
# This is required to make command available in PyMOL
#pymol.cmd.extend("color_consurf_adjusted", color_consurf_adjusted)

def color_structure(main_directory, score_types, cutoffs, dict_genes, pdb):
    '''
    Recolors the structure given, for all aligned isoforms, and all datatypes available. Saves pymol session files.
    '''
    #import pymol
    #pymol.finish_launching(['pymol', '-cq'])
    #load pdb
    pymol.cmd.load(pdb)
    #make picture in standard format, delete all alternative confirmations, turn off deth_cue, turn on seq_view and show structure as a cartoon.
    pymol.cmd.set('depth_cue', 0)
    pymol.cmd.set('seq_view', 1)
    pymol.cmd.hide('lines')
    pymol.cmd.show('cartoon')
    pymol.cmd.alter('all', 'b=-10')
    pymol.cmd.remove('not alt +A')
    pymol.cmd.set('specular', 0)
    #define name pdb
    pdb_split = pdb.split('/')[-1]
    pdb_id = pdb_split.split('.')[0].lower()
    #path to b-factors for this pdb
    path = os.path.join(main_directory, 'B-factors', pdb_id.upper())
    if os.path.exists(path):
        all_chains = os.listdir(path)
        amount_chains = len(all_chains)
        for datatype in score_types:
            all_combinations = []
            list_files = []
            for chain in all_chains:
                for file in os.listdir(os.path.join(path,chain)):
                    if 'LOESS' in datatype:
                        if datatype in file and 'LOESS' in file:
                            list_files.append(os.path.join(path,chain,file))
                    else:
                        if datatype in file and not 'LOESS' in file:
                            list_files.append(os.path.join(path,chain,file))
            
            for combo in itertools.combinations(list_files, amount_chains):
                combo = list(combo)
                i = 0
                bool = True
                while i < amount_chains and bool == True:
                    matching = [s for s in combo if ('_'+all_chains[i]+'_') in s]
                    if len(matching) == 1:
                        i = i+1
                        bool = True
                    else:
                        bool = False
                if bool == True:
                    all_combinations.append(combo)
            for combination in all_combinations:
                all_protiso = str()
                all_genes = str()
                for ifile in combination:
                    chain = ifile.split('_')[-3]
                    ENSPID = ifile.split('_')[-2]
                    load_newB(ifile, 
                    chain_selection='chain '+chain)
                    gene = dict_genes[ENSPID][0]
                    if not ENSPID in all_protiso:
                        all_protiso = all_protiso + '_' + ENSPID
                        all_genes = all_genes +'_' +gene

                alphaToAll("all", 'b')
                minimum = cutoffs[datatype][0][0]
                maximum = cutoffs[datatype][0][1]
                color_consurf_adjusted(min=minimum,max=maximum)
                folder = pdb_id.upper()
                file_name = os.path.join(main_directory, 'structures',folder, pdb_id+'_'+all_genes+'_'+all_protiso+'_'+datatype+'_9bins.pse')
                pymol.cmd.save(file_name)

                minimum = cutoffs[datatype][1][0]
                maximum = cutoffs[datatype][1][1]
                color_consurf_adjusted(min=minimum, max=maximum, amount_bins='17')
                file_name = os.path.join(main_directory,'structures', folder, pdb_id+'_'+all_genes+'_'+all_protiso+'_'+datatype+'_17bins.pse')
                pymol.cmd.save(file_name)
    pymol.cmd.delete(pdb_id)
    #pymol.cmd.quit()
    logging.info("Done coloring {}".format(pdb_id))
