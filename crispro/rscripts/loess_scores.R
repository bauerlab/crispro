#!/usr/bin/env Rscript
library(ggplot2)
library(readr)
library(dplyr)
library(purrr)
library(tidyr)
library(stringr)

args <- commandArgs(trailingOnly = TRUE)

df_crispro <- read_csv(args[1])
formula <- "position"
if(length(args) == 2)
  formula <- args[2]

df_ctrl <- df_crispro %>%
  filter(control == "positive" | control == "negative")
df_crispro <- df_crispro %>%
  filter((control != "positive" & control != "negative") | (is.na(control)))
df_loess <- data.frame()
control_idx <- match("control", colnames(df_crispro))
num_cols <- length(colnames(df_crispro))
first_score <- colnames(df_crispro)[control_idx + 1]
scores <- colnames(df_crispro)[(control_idx + 1):num_cols]

formula_vector <- unname(sapply(strsplit(formula, '+', fixed = TRUE)[[1]], trimws))
for (txn in unique(df_crispro$ensembl_transcript_id)){
  df_txn_loess <- df_crispro[which(df_crispro$ensembl_transcript_id == txn), ]
  df_txn_raw <- df_txn_loess[complete.cases(df_txn_loess[[first_score]]), ]
  if (nrow(df_txn_raw) / nrow(df_txn_loess) < .2) next
  num_guides <- nrow(df_txn_raw)

  sp <- min(1, 100 / (max(df_txn_loess$position, na.rm = TRUE) - 1)) # stop codon
  aas <- min(df_txn_raw$position):max(df_txn_raw$position)

  df <- df_txn_loess %>% filter(!duplicated(position), position >= min(aas), position <= max(aas)) %>%
    select_(.dots = formula_vector)
  new_cols <- c()
  score_cols <- c()
  loess_cols <- c()
  for (i in seq(ncol(df_crispro))){
    col = colnames(df_crispro)[i]
    new_cols <- c(new_cols, col)
    if (i > control_idx){
      loess_cols <- c(loess_cols, sprintf("%s_LOESS", col))
      df[[sprintf("%s_LOESS", col)]] <- predict(loess(as.formula(sprintf("`%s` ~ position",col)),
                                                    data = df_txn_raw, span = sp, family = 'g', surface = 'd'),
                                              newdata = aas)
      if (nrow(df_ctrl) > 0){
        df_ctrl[, sprintf("%s_LOESS", col)] <- NA
      }
      new_cols <- c(new_cols, sprintf("%s_LOESS", col))
    }
  }
  df_txn_loess <- merge(df_txn_loess, df[, c('position', loess_cols)], by = "position", all.x = TRUE)
  df_loess <- rbind(df_loess, df_txn_loess)
}
if (nrow(df_ctrl) > 0)
  df_loess <- rbind(df_loess, df_ctrl)
df_loess <- df_loess %>%
  select(new_cols)
write_csv(df_loess, sprintf("%s/crispro_scores_annotated-loess.csv", dirname(args[1])))
