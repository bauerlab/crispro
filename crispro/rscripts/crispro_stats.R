#!/usr/bin/env Rscript
#############################################################
# crispro_stats.R
# Provides statistical analyses and visualization of CRISPR
# score and peptide annotations
#############################################################
library(ggplot2)
library(readr)
library(dplyr)
library(purrr)
library(lazyeval)
library(gridExtra)

args <- commandArgs(trailingOnly = TRUE)
df <- read_csv(args[1], col_types = cols(provean_score = col_double(), Exon = col_character(), disorder_score = col_double(), .default = col_guess()))
isMite <- as.logical(toupper(args[2]))
if (!isMite){
  df <- df %>%
    distinct(guide, transcript_name, .keep_all = TRUE)
} else {
  df <- df %>%
    distinct(position, transcript_name, .keep_all = TRUE)
}
control_idx <- match("control", colnames(df))
num_cols <- length(colnames(df))

emd <- function(test,baseline){
  # Calculates Earth Mover's Distance between two distributions
  # adapted from: https://en.wikipedia.org/wiki/Earth_mover%27s_distance
  # Args:
  #   test: vector of crispr scores for gene to test
  #   baseline: vector of crispr scores from nontargeting/neutral (null) set
  #
  # Returns:
  #   Earth Mover's Distance between test and null distribution
  score_range <- range(test, baseline, na.rm = TRUE)
  density_baseline <- density(baseline, from = score_range[1], to = score_range[2])
  density_test <- density(test, from = score_range[1], to = score_range[2])
  dirt <- accumulate(density_test$y - density_baseline$y, `+`) %>% abs() %>% sum()
  return(dirt)
}

summaryStats <- function(df, score, isMite){
  # Provides summary statistics (IQR,mean, sd, EMD) for each peptide in crispro dataset
  # Args:
  #   df: crispro dataframe with crispro scores
  #   score: name of score to analyze
  # Returns:
  #    Dataframe of summary statistics
  if(df %>% filter(control == "negative", !is.na(guide)) %>% nrow() > 0){
    neutral_scores <- df %>% filter_("control=='negative'", sprintf("!is.na(`%s`)", score)) %>%
      .[[score]]
  } else
    neutral_scores <- df %>% mutate_(zscore = interp(~ scale(x), x = as.name(score))) %>%
    filter_("abs(zscore) < 2", sprintf("!is.na(`%s`)", score)) %>% .[[score]]
  df_stats <- df %>%
    filter_(sprintf("!is.na(`%s`)", score), "control=='Library'")
  df_stats <- df_stats %>%
    group_by(ensembl_gene_id, ensembl_transcript_id, ensembl_peptide_id,
             gene_name, transcript_name, APPRIS) %>% filter(n() > 10) %>%
    summarise_(mean = interp(~ mean(x), x = as.name(score)),
               std_dev = interp(~ sd(x), x = as.name(score)),
               num_guides = interp(~ length(x), x = as.name(score)),
               q1 = interp(~ quantile(x, .25), x = as.name(score)),
               median = interp(~ median(x), x = as.name(score)),
               q3 = interp(~ quantile(x, .75), x = as.name(score)),
               iqr = interp(~ IQR(x), x = as.name(score)),
               earth_movers_distance = interp(~ emd(x, neutral_scores), x = as.name(score)))
  df_stats <- df_stats %>% mutate(isHit = ((q1 * q3) > 0)) %>%
    arrange(desc(earth_movers_distance))
  return(df_stats)
}

violinHits <- function(df, score, max_peps = 18, pep_order = c()){
  # Plots violin plots of peptides with a max of 20 per row
  #
  # Args:
  #   df: CRISPRO Data FRAME containing only genes that should be plotted
  #   score: CRISPR score to plot against
  #
  # Returns:
  #   gtable
  # order data frame by peptide median score
  if (length(pep_order) == 0){
    df_med_score <- df %>% filter(control == 'Library') %>% group_by(transcript_name) %>%
      summarise_(median_score = interp(~ median(x, na.rm = TRUE), x = as.name(score)) ) %>%
      arrange(median_score)
    pep_order <- df_med_score[["transcript_name"]]
  }
  controls <- df %>% filter(control != 'Library') %>% select(transcript_name) %>%
    unique() %>% .[["transcript_name"]]
  plts <- list()
  num_rows <- as.integer(length(pep_order) / max_peps + 1)
  rng <- range(df[[score]], na.rm = TRUE)
  lay <- rbind(c(1, 1), c(2, NA))
  rem <- length(pep_order) %% max_peps
  for (i in 1:num_rows){
    peps <- pep_order[((i - 1) * max_peps + 1):min(i * max_peps, length(pep_order))]
    print(peps)
    lvls <- c(controls,peps)
    plt <- ggplot(df %>% filter(transcript_name %in% lvls),
                  aes(x = factor(transcript_name, levels = lvls, ordered = TRUE))) +
      geom_violin(aes_string(y = sprintf("`%s`", score)), draw_quantiles = c(0.25,0.5,0.75), na.rm = TRUE) +
      geom_hline(yintercept = 0) + theme_classic() + ylim(rng[1], rng[2]) +
      labs(title = sprintf("%s Distribution per Transcript", score), x = "Transcript", y = score) +
      theme(axis.text.x = element_text(angle = -90, hjust = 1))
    if (i == 1)
      plts <- ggplotGrob(plt)
    else if (i == num_rows)
      plts <- arrangeGrob(plts, plt, nrow = 2, ncol = 2, heights = c(num_rows-1, 1), widths = c(rem + 1, max_peps - rem),
                          layout_matrix = lay)
    else
      plts <- gtable_rbind(plts, ggplotGrob(plt))
  }
  return(plts)
}
max_peps <- 18
scores <- colnames(df)[(control_idx + 1):num_cols]
df[, scores] <- sapply(df[, scores], as.numeric)
num_peps <- df$transcript_name %>% unique() %>% length()
violinHeight <- (as.integer(num_peps / max_peps) + 1) * 4
violinWidth <- as.integer(max_peps * 8.5 / 18) + 1
map(scores,
    function(x) write_csv(summaryStats(df, x, isMite),
                          path = file.path(dirname(args[1]), "score_summary", sprintf("crispro_stats_%s.csv", x))))
map(scores,
    function(x) ggsave(sprintf("%s_scores_violinPlot.pdf", x), plot = violinHits(df, x),
                               path = file.path(dirname(args[1]), "score_summary"),
                       height = violinHeight, width = violinWidth, units = "in", limitsize = FALSE))