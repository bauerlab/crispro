# -*- coding: utf-8 -*-
"""
Created on Fri Sep  1 08:49:57 2017

"""
import os, subprocess, gzip, logging, sys
import matplotlib as mpl

mpl.use("Agg")
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats

# CRISPRO_DIR = os.path.dirname(__file__)


def loadCrispro(ids, id_type, annot_file):
    iter_csv = pd.read_csv(annot_file, iterator=True, chunksize=10**7)
    df_crispro = pd.concat(
        [df_chunk[df_chunk[id_type].isin(ids)] for df_chunk in iter_csv]
    )
    return df_crispro


def readDF(file):
    if file.endswith(".txt") or file.endswith(".tsv"):
        return pd.read_table(file)
    elif file.endswith(".csv"):
        return pd.read_csv(file)
    elif file.endswith(".xls") or file.endswith(".xlsx"):
        return pd.read_excel(file)
    else:
        return None


def cutadapt(adapter, outdir, file):
    cutadapt_log = file.split(".fastq")[0] + ".log"
    trimmed_file = os.path.join(
        outdir, os.path.basename(file.replace(".fastq", "-trimmed.fastq"))
    )
    cmd = "cutadapt -g {} --discard-untrimmed -o {} {} ".format(
        adapter, trimmed_file, file
    ) + "> {} 2>&1".format(cutadapt_log)
    logging.info("Performing Cutadapt on {}".format(file))
    ret = subprocess.call(cmd, shell=True)
    if ret != 0:
        logging.error("Cutadapt error on file: {}".format(file))
        sys.exit(ret)


def gini(counts):
    """
    Adapted from MAGeCK from the Shirley Liu Lab
    """
    xs = sorted(np.log(counts + 1))
    n = len(xs)
    gssum = sum([(i + 1.0) * xs[i] for i in range(n)])
    ysum = sum(xs)
    if not ysum:
        ysum = 1.0
    return 1 - 2.0 * (n - gssum / ysum) / (n - 1)


def guide_counter(lib, sgrna_file):
    try:
        if sgrna_file.endswith(".gz"):
            infile = gzip.open(sgrna_file, "r")
        else:
            infile = open(sgrna_file, "r")
    except:
        e = sys.exc_info()[0]
        logging.error("{} in opening {}".format(e, sgrna_file))
        sys.exit(1)
    counter = dict()
    for guide in lib:
        counter[guide] = 0
    count = 0
    guide_len = 20
    logging.info("Counting guides in file {}".format(sgrna_file))
    for line in infile:
        if count % 4 == 1:
            line = line.strip()
            if len(line) >= guide_len:
                guide = line[:guide_len]
                if guide in lib:
                    counter[guide] += 1
        count += 1
    infile.close()
    df_counter = pd.DataFrame.from_dict(counter, orient="index")
    mapped_reads = df_counter.iloc[:, 0].sum()
    stats = df_counter.iloc[:, 0].describe(percentiles=[0.1, 0.5, 0.9]).tolist()
    total_reads = int(count / 4)
    mapped_percent = float(mapped_reads) / total_reads
    gini_score = gini(df_counter.iloc[:, 0])
    final_stats = [total_reads, mapped_reads, mapped_percent, gini_score] + stats[1:]
    return df_counter, final_stats


def fileReader(file):
    if file.endswith(".gz"):
        return gzip.open
    else:
        return open


def getIdentifier(identifiers):
    if os.path.isfile(identifiers[0]):
        df_ids = readDF(identifiers[0])
        ids = df_ids[df_ids.columns[0]].tolist()
        id0 = ids[0]
    else:
        id0 = identifiers[0]
        ids = identifiers
    if id0.startswith("ENSG"):
        id_type = "ensembl_gene_id"
    elif id0.startswith("ENST"):
        id_type = "ensembl_transcript_id"
    elif id0.startswith("ENSP"):
        id_type = "ensembl_peptide_id"
    else:
        id_type = "gene_name"
    return id_type, list(set(ids))


def colored_sequences(
    outdir, subplots, amount_plots, score_types, df_temp, transcript, cutoffs, gene_name
):
    # define crispro colors
    crispro_colors9 = [
        "#045a8d",
        "#2b8cbe",
        "#74a9cf",
        "#bdc9e1",
        "#fff7f3",
        "#fbb4b9",
        "#f768a1",
        "#c51b8a",
        "#7a0177",
    ]
    crispro_colors17 = [
        "#023858",
        "#045a8d",
        "#0570b0",
        "#3690c0",
        "#74a9cf",
        "#a6bddb",
        "#d0d1e6",
        "#ece7f2",
        "#fff7f3",
        "#fde0dd",
        "#fcc5c0",
        "#fa9fb5",
        "#f768a1",
        "#dd3497",
        "#ae017e",
        "#7a0177",
        "#49006a",
    ]
    # make cmap of crispro colors
    cmap_9bins = ListedColormap(sns.color_palette(crispro_colors9, n_colors=9).as_hex())
    cmap_17bins = ListedColormap(
        sns.color_palette(crispro_colors17, n_colors=17).as_hex()
    )
    cols = list(score_types)
    cols = ["AA", "codon"] + cols
    # cut dataframe so it just contains aminoacids with corresponding LOESS, raw enrichment, LOESS, and raw dropout scores.
    df_temp = df_temp[cols].reset_index(drop=True)
    df_temp.index = np.arange(1, len(df_temp) + 1)
    # transpose complete dataframe, so the heatmaps will be horizontal presentations of the sequence
    df_transposed = df_temp.T
    # define array and dataframe with the amino acid sequence (keys) itself.
    df_keys = df_transposed.loc[["AA"], :]
    df_keys = df_keys.astype(str)
    df_codon = df_transposed.loc[["codon"], :]
    df_codon = df_codon.astype(str)
    # make df with only zero's (as value for 'AA')
    df_key_data = df_transposed.loc[[score_types[0]], :]
    for col in df_key_data.columns:
        df_key_data[col].values[:] = 0
    # for i in range(len(df_key_data.columns)):
    # df_key_data.set_value(score_types[0], i, 0.0)
    #    df_key_data.at[score_types[0], i] = 0.0
    df_key_data = df_key_data.astype(float)
    df_codon_data = df_key_data
    # there's two cmap options
    cmaps = 2
    for c in range(cmaps):
        if c == 0:
            my_cmap = cmap_17bins
            amount_bins = "_17bins"
        else:
            my_cmap = cmap_9bins
            amount_bins = "_9bins"
        # set masking color for n/a values
        my_cmap.set_bad(color="#ffeda0")
        reps = (len(df_key_data.columns) // 50) + 1
        start = -50
        end = 0
        with PdfPages(
            os.path.join(
                outdir,
                "colored_sequence_"
                + gene_name
                + "_"
                + transcript
                + amount_bins
                + ".pdf",
            )
        ) as pdf:
            for i in range(reps):
                ind = int(1)
                if i < (reps - 1):
                    start = start + 50
                    end = end + 50
                    # define size figure
                    fig, (subplots) = plt.subplots(
                        amount_plots, sharex=True, sharey=True, figsize=(34, 2.5)
                    )
                else:
                    start = start + 50
                    end = (len(df_key_data.columns)) - 1
                    size = ((end - start) + 1) * 0.68
                    # define size figure
                    fig, (subplots) = plt.subplots(
                        amount_plots, sharex=True, sharey=True, figsize=(size, 2.5)
                    )
                    # make heatmaps
                for score in score_types:
                    if c == 0:
                        min = cutoffs[score][1][0]
                        max = cutoffs[score][1][1]
                    else:
                        min = cutoffs[score][0][0]
                        max = cutoffs[score][0][1]
                    ind = int(ind + 1)
                    # define seperate df for all data
                    df_score = df_transposed.loc[[score], :]
                    # convert data to a float, with one decimal
                    df_score = df_score.astype(float).round(1)
                    sns.heatmap(
                        df_score.iloc[:, start:end],
                        ax=subplots[ind],
                        vmin=min,
                        vmax=max,
                        cmap=my_cmap,
                        annot=True,
                        linewidths=0.01,
                        xticklabels=True,
                        cbar=False,
                    )
                sns.heatmap(
                    df_key_data.iloc[:, start:end],
                    ax=subplots[0],
                    vmin=min,
                    vmax=max,
                    cmap=my_cmap,
                    annot=df_keys.iloc[:, start:end],
                    fmt="",
                    linewidths=0.01,
                    cbar=False,
                )
                sns.heatmap(
                    df_codon_data.iloc[:, start:end],
                    ax=subplots[1],
                    vmin=min,
                    vmax=max,
                    cmap=my_cmap,
                    annot=df_codon.iloc[:, start:end],
                    fmt="",
                    linewidths=0.01,
                    cbar=False,
                )
                j = 0
                for ax in fig.axes:
                    plt.sca(ax)
                    ax.get_yaxis().set_ticks([])
                    ax.set_ylabel(cols[j], rotation="horizontal", labelpad=80)
                    # ax.yaxis.set_label_coords(-0.05, 0.45)
                    ax.xaxis.set_ticks_position("none")
                    j = int(j + 1)
                # remove space between plots, make space for x-axis numbering, rotate y labels
                plt.gcf().subplots_adjust(bottom=0.3)
                plt.tight_layout()
                fig.subplots_adjust(hspace=0)
                # save figure and close plot
                # complete_figure.add_subplot(fig)
                pdf.savefig(fig)
                plt.close()


def distribution(df_crispro, score_type, outdir):
    quant_95 = df_crispro[score_type].quantile(0.95)
    quant_5 = df_crispro[score_type].quantile(0.05)
    cutoff = max([abs(quant_95), abs(quant_5)])
    fig, ax = plt.subplots()
    sns.displot(df_crispro[score_type].dropna())
    ax.axvline(0, linestyle="--", color="k")
    ax.axvline(quant_5, linestyle="--", color="k")
    ax.axvline(quant_95, linestyle="--", color="k")
    ax.axvline(cutoff, linestyle=":", color="r")
    ax.axvline((cutoff * -1), linestyle=":", color="r")
    cutoff1 = mpl.patches.Patch(color="red", label=round(cutoff, 2), lw=2.0)
    cutoff2 = mpl.patches.Patch(color="red", label=round((cutoff * -1), 2), lw=2.0)
    patch5 = mpl.patches.Patch(
        color="black", label="5% = " + str(round(quant_5, 2)), lw=2.0
    )
    patch95 = mpl.patches.Patch(
        color="black", label="95% = " + str(round(quant_95, 2)), lw=2.0
    )
    binsize9 = (cutoff * 2.0) // 7
    binsize17 = (cutoff * 2.0) // 15
    patch_9 = mpl.patches.Patch(
        color="white", label="binsize (9)= " + str(round(binsize9, 2))
    )
    patch_17 = mpl.patches.Patch(
        color="white", label="binsize (17)= " + str(round(binsize17, 2))
    )
    lgd = ax.legend(
        handles=[patch5, patch95, cutoff1, cutoff2, patch_9, patch_17],
        bbox_to_anchor=(1.05, 1),
        loc=2,
        borderaxespad=0.0,
    )
    plt.ylabel("kernel density estimation (KDE)")
    outfigure = os.path.join(outdir, "data_distribution_" + score_type + ".png")
    fig.savefig(outfigure, bbox_extra_artists=(lgd,), bbox_inches="tight")
    plt.close()
    cutoffs_9bin = [((cutoff * -1) - binsize9), (cutoff + binsize9)]
    cutoffs_17bin = [((cutoff * -1) - binsize17), (cutoff + binsize17)]
    return cutoffs_9bin, cutoffs_17bin, binsize9, binsize17


def principal_iso(df_crispro, principal_isoforms):
    for gene, group in df_crispro.groupby("ensembl_gene_id"):
        if not group.ensembl_transcript_id.isin(principal_isoforms).any():
            dict_isoforms = {}
            for transcript, subgroup_txn in group.groupby("ensembl_transcript_id"):
                df_temp = subgroup_txn.reset_index(drop=True)
                if not df_temp.APPRIS.isnull().values.any():
                    dict_isoforms[df_temp.APPRIS[0]] = transcript
                else:
                    try:
                        dict_isoforms["No"] = dict_isoforms["NA"].append(transcript)
                    except KeyError:
                        dict_isoforms["No"] = [transcript]
            # APPRIS annotations ['principal1', 'alternative2', 'principal4', 'principal3', 'principal2', 'alternative1', 'principal5']
            # if 'principal1' in dict_isoforms.keys():
            if "PRINCIPAL:1" in list(dict_isoforms.keys()):
                principal_isoforms.append(dict_isoforms["PRINCIPAL:1"])
            elif "PRINCIPAL:2" in list(dict_isoforms.keys()):
                principal_isoforms.append(dict_isoforms["PRINCIPAL:2"])
            elif "PRINCIPAL:3" in list(dict_isoforms.keys()):
                principal_isoforms.append(dict_isoforms["PRINCIPAL:3"])
            elif "PRINCIPAL:4" in list(dict_isoforms.keys()):
                principal_isoforms.append(dict_isoforms["PRINCIPAL:4"])
            elif "PRINCIPAL:5" in list(dict_isoforms.keys()):
                principal_isoforms.append(dict_isoforms["PRINCIPAL:5"])
            elif "ALTERNATIVE:1" in list(dict_isoforms.keys()):
                principal_isoforms.append(dict_isoforms["ALTERNATIVE:1"])
            elif "ALTERNATIVE:2" in list(dict_isoforms.keys()):
                principal_isoforms.append(dict_isoforms["ALTERNATIVE:2"])
            elif "MINOR" in list(dict_isoforms.keys()):
                principal_isoforms.append(dict_isoforms["MINOR"])
            elif "No" in list(dict_isoforms.keys()):
                principal_isoforms.extend(dict_isoforms["No"])
    return principal_isoforms


def plot_annotations(df_hits, score, outdir):
    df_hits = df_hits[df_hits["guide"].notnull()].copy()
    df_hits_pos = df_hits.groupby("transcript_name").filter(
        lambda g: g[score].median() > 0
    )
    df_hits_neg = df_hits[
        ~df_hits["transcript_name"].isin(df_hits_pos["transcript_name"].unique())
    ]

    def getGroupedScores(x, y):
        df = pd.DataFrame(list(zip(x, y)), columns=["annot", "score"])
        groups = df[df["annot"].notnull()].groupby("annot")["score"]
        grouped_scores = [vals.values for _, vals in groups]
        return grouped_scores

    def plotHits(annot, score, df_hits, name, plotType="scatter", xlim=None):
        if len(df_hits["transcript_name"].unique()) <= 1:
            return
        if plotType == "scatter":
            if df_hits[df_hits[score].notnull()].shape[0] < 10**4:
                fig = sns.jointplot(
                    x=annot, y=score, data=df_hits, xlim=xlim, alpha=0.2
                )
            else:
                fig = sns.jointplot(
                    x=annot, y=score, kind="kde", data=df_hits, xlim=xlim
                )
            spear_cor, spear_p = stats.spearmanr(
                df_hits[annot].values, df_hits[score].values
            )
            fig.ax_joint.plot(
                [],
                [],
                label="spearmanr = {:.2f}; p = {:.3g}".format(spear_cor, spear_p),
            )
            fig.ax_joint.legend(loc="best")
            title = "{} vs {} ({} hits)".format(score, annot, name)
            path = os.path.join(
                outdir, "{}_vs_{}_{}_scatter.pdf".format(score, annot, name)
            )
            plt.subplots_adjust(top=0.8)
            plt.suptitle(title)
        else:  # violin
            path = os.path.join(
                outdir, "{}_vs_{}_{}_violin.pdf".format(score, annot, name)
            )
            # add pval
            groups = df_hits[df_hits[annot].notnull()].groupby(annot)[score]
            grouped_scores = [vals.values for _, vals in groups]
            if len(grouped_scores) > 2:
                _, pval = stats.kruskal(*grouped_scores, nan_policy="omit")
                annotation = r"kruskal-wallis, $p = {:.2g}$".format(pval)
            elif len(grouped_scores) == 2:
                _, pval = stats.mannwhitneyu(*grouped_scores)
                annotation = r"mann-whitney, $p = {:.2g}$".format(pval)
            else:
                annotation = r"$p = NA$"
            fig, ax = plt.subplots()
            try:
                sns.violinplot(
                    x=annot,
                    y=score,
                    data=df_hits,
                    palette="hls",
                    inner="quartile",
                    ax=ax,
                    hue=annot,
                    legend=False,
                )
            except ValueError:
                logging.warning("{} has missing values for gene set".format(annot))
            ax.plot([], [], linestyle="", alpha=0, label=annotation)
            title = "{} vs {} ({} hits)".format(score, annot, name)
            plt.title(title)
            plt.legend(loc="best", markerscale=0, fontsize="x-small")
        fig.savefig(path)
        plt.close()

    def plotFacet(
        annot, score, df_hits, plotType="scatter", facet="transcript", **facet_kwargs
    ):
        fig = sns.FacetGrid(df_hits, **facet_kwargs)
        if plotType == "scatter":

            def annotatedScatter(x, y, **kwargs):
                rho, pval = stats.spearmanr(x, y, nan_policy="omit")
                annotation = r"$\rho = {:.2g}$; p = ${:.2g}$".format(rho, pval)
                plt.scatter(x, y, alpha=0.2, label=annotation, **kwargs)

            fig.map(annotatedScatter, annot, score).set_titles("{col_name}")

        else:  # violin

            def annotatedViolin(x, y, **kwargs):
                grouped_scores = getGroupedScores(x, y)
                if len(grouped_scores) > 2:
                    _, pval = stats.kruskal(*grouped_scores, nan_policy="omit")
                    annotation = r"kruskal-wallis, $p = {:.2g}$".format(pval)
                elif len(grouped_scores) == 2:
                    _, pval = stats.mannwhitneyu(*grouped_scores)
                    annotation = r"mann-whitney, $p = {:.2g}$".format(pval)
                else:
                    annotation = r"$p = NA$"
                if not x.isnull().all() and not y.isnull().all():
                    ax = sns.violinplot(
                        x=x,
                        y=y,
                        alpha=0.2,
                        palette="hls",
                        inner="quartile",
                        hue=x,
                        legend=False,
                    )
                    ax.plot([], [], linestyle="", alpha=0, label=annotation)

            fig.map(annotatedViolin, annot, score).set_titles("{col_name}")

        for ax in fig.axes.flatten():
            ax.legend(loc="best", markerscale=0, fontsize="x-small")

        plt.subplots_adjust(top=0.9)
        plt.suptitle("{} vs {}".format(score, annot))
        outfigure = os.path.join(
            outdir, "{}_vs_{}_{}_{}.pdf".format(score, annot, facet, plotType)
        )
        plt.savefig(outfigure)
        plt.close()

    scatters = [
        "provean_score",
        "offtarget_score",
        "doench_score",
        "oof_score",
        "distance_3_exon_border",
        "distance_5_exon_border",
        "disorder_score",
        "targeted_transcripts_frac",
    ]
    xlims = [None] * 4 + [(-1, 200)] * 2 + [(0, 1.1)] * 2
    scatter_args = list(zip(scatters, xlims))
    for annot, xlim in scatter_args:
        if not df_hits_pos.empty:
            plotHits(annot, score, df_hits_pos, "positive", xlim=xlim)
        if not df_hits_neg.empty:
            plotHits(annot, score, df_hits_neg, "negative", xlim=xlim)
        if not df_hits.empty:
            plotFacet(
                annot,
                score,
                df_hits,
                xlim=xlim,
                col="transcript_name",
                col_wrap=2,
                height=4,
            )
    df_hits["In Domain"] = df_hits["Interpro_Description"].notnull()
    df_hits_pos["In Domain"] = df_hits_pos["Interpro_Description"].notnull()
    df_hits_neg["In Domain"] = df_hits_neg["Interpro_Description"].notnull()
    secStruct_dict = {"C": "coil", "H": "helix", "E": "sheet"}
    df_hits["Secondary Structure"] = df_hits["SecStruct"].apply(
        lambda x: secStruct_dict[x] if x in secStruct_dict else np.nan
    )
    df_hits_pos["Secondary Structure"] = df_hits_pos["SecStruct"].apply(
        lambda x: secStruct_dict[x] if x in secStruct_dict else np.nan
    )
    df_hits_neg["Secondary Structure"] = df_hits_neg["SecStruct"].apply(
        lambda x: secStruct_dict[x] if x in secStruct_dict else np.nan
    )
    aa_order_pos = (
        df_hits_pos.groupby("AA")[score].agg("median").sort_values().index.values
    )
    aa_cat_type_pos = pd.api.types.CategoricalDtype(
        categories=aa_order_pos, ordered=True
    )
    df_hits_pos["AA"] = df_hits_pos["AA"].astype(aa_cat_type_pos)
    aa_order_neg = (
        df_hits_neg.groupby("AA")[score].agg("median").sort_values().index.values
    )
    aa_cat_type_neg = pd.api.types.CategoricalDtype(
        categories=aa_order_neg, ordered=True
    )
    df_hits_neg["AA"] = df_hits_neg["AA"].astype(aa_cat_type_neg)
    categoricals = [
        "Exon_Multiple_of_3",
        "In Domain",
        "Secondary Structure",
        "escapeNMD",
        "AA",
        "codon",
    ]
    for annot in categoricals[:-1]:
        if not df_hits_pos.empty:
            plotHits(annot, score, df_hits_pos, "positive", plotType="violin")
        if not df_hits_neg.empty:
            plotHits(annot, score, df_hits_neg, "negative", plotType="violin")

    facets = ["transcript_name"] * len(categoricals[:-2])
    facet_args = list(zip(categoricals[:-2] + ["codon"], facets))
    if not df_hits.empty:
        for annot, col in facet_args:
            plotFacet(
                annot, score, df_hits, plotType="violin", col=col, col_wrap=2, height=4
            )

    if not df_hits_neg.empty:
        plotFacet(
            "codon",
            score,
            df_hits_neg,
            plotType="violin",
            facet="negative",
            col="AA",
            col_wrap=4,
            sharex=False,
        )
    if not df_hits_pos.empty:
        plotFacet(
            "codon",
            score,
            df_hits_pos,
            plotType="violin",
            facet="positive",
            col="AA",
            col_wrap=4,
            sharex=False,
        )
    stat_data = []
    for scatter in scatters:
        for df, name in zip([df_hits_pos, df_hits_neg], ["positive", "negative"]):
            rho, pval = stats.spearmanr(df[scatter], df[score], nan_policy="omit")
            stat_data.append(["all_hits", name, scatter, "spearman rho", rho, pval])
    for cat in categoricals:
        for df, name in zip([df_hits_pos, df_hits_neg], ["positive", "negative"]):
            grouped_scores = getGroupedScores(df[cat], df[score])
            if len(grouped_scores) > 2:
                stat, pval = stats.kruskal(*grouped_scores, nan_policy="omit")
                stat_name = "kruskal statistic"
            elif len(grouped_scores) == 2:
                stat, pval = stats.mannwhitneyu(*grouped_scores)
                stat_name = "mann-whitney statistic"
            else:
                stat, pval = (np.nan, np.nan)
                stat_name = ""
            stat_data.append(["all_hits", name, cat, stat_name, stat, pval])
    for txn, group in df_hits.groupby("transcript_name"):
        if group[score].median() > 0:
            name = "positive"
        else:
            name = "negative"
        for scatter in scatters:
            rho, pval = stats.spearmanr(group[scatter], group[score], nan_policy="omit")
            stat_data.append([txn, name, scatter, "spearman rho", rho, pval])
        for cat in categoricals:
            grouped_scores = getGroupedScores(group[cat], group[score])
            if len(grouped_scores) > 2:
                stat, pval = stats.kruskal(*grouped_scores, nan_policy="omit")
                stat_name = "kruskal statistic"
            elif len(grouped_scores) == 2:
                stat, pval = stats.mannwhitneyu(*grouped_scores)
                stat_name = "mann-whitney statistic"
            else:
                stat, pval = (pd.np.nan, pd.np.nan)
            stat_data.append([txn, name, cat, stat_name, stat, pval])
    df_stats = pd.DataFrame(
        stat_data,
        columns=[
            "transcript",
            "hit_class",
            "annotation",
            "statistic name",
            "statisitic",
            "pval",
        ],
    )
    df_stats.sort_values(by=["annotation", "transcript"], inplace=True)
    df_stats.to_csv(
        os.path.join(outdir, "{}_annotation_stats.csv".format(score)), index=False
    )
