CRISPRO
=======

.. image:: ./CRISPROlogo.png
  :alt: CRISPRO logo

Analysis and Visualization of Saturating Mutagenesis CRISPR Screens
-------------------------------------------------------------------

CRISPRO is a pipeline to process next generation sequnecing data from
saturating mutagenesis CRISPR screens, whereby sgRNAs are mapped to
amino acids of the protein, allowing for the inference of functional
scores for amino acids in a protein. This scoring is coupled with
visualization at the primary sequence level with graphs of scores and
tracks containing domain structure and at the tertiary structure level,
where peptide fragments are aligned to existing PDB structures and
recolored in a heatmap style reflecting functional scores of amino
acids.

-  Jump to `Usage <#usage>`__
-  Jump to `Output Summary <#output>`__

Installation
------------

CRISPRO requires python >3.9 and R >4.2.1

Docker
~~~~~~~~~~~~
There is now a docker image available with the python3 version of CRISPRO. If you would like to run CRISPRO on a M chip based MacOS  environment, this is the only way currently (as pymol is not available via conda for this, as well as deseq2). Warning: currently the docker image is under construction, and is still very large. It can be found `here <https://hub.docker.com/repository/docker/vivienschoonenberg/crispro/general>`__.


Install manually
~~~~~~~~~~~~

A linux environment is needed. Currently there is known issues with parallelization on MacOS, as well as with installation of pymol via conda with the new apple M2 chips.

**NOTE:** You now can run CRISPRO on MacOS powered by M chips. You'll just have to make sure you set up your (crispro) conda environment mimicking an intel chip, by running the following:
   .. code:: bash

      conda config --env --set subdir osx-64

   ..

R packages
^^^^^^^^^^

-  egg
-  gridExtra
-  tidyverse
    -  ggplot2
    -  dplyr
    -  lazyeval
    -  purrr
    -  RColorBrewer
    -  readr
-  DESeq2 (optional)

Python packages
^^^^^^^^^^^^^^^

-  pandas (version 0.21.0 or newer)
-  numpy
-  seaborn
-  matplotlib 
-  pymol (version 2.5 or newer)
-  scipy
-  biopython

   1. Install `miniconda/anaconda <http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh>`__

   .. code:: bash

      wget -c http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86\\\_64.sh
      chmod +x Miniconda3-latest-Linux-x86\_64.sh
      ./Miniconda2-latest-Linux-x86\_64.sh

   ..

   1. Create CRISPRO virtual environment

   .. code:: bash

      conda create --name crispro
      source activate crispro
      conda config --add channels r
      conda config --add channels defaults
      conda config --add channels bioconda
      conda config --add channels conda-forge
   ..


   2. Install pymol

   - Install PyMOL with conda (see `pymol.org <http://pymol.org/>`__)

   .. code:: bash

      conda install -c schrodinger pymol

   ..


   3. Install dependencies via conda

   .. code:: bash

      conda install pandas numpy matplotlib seaborn biopython r-base r-essentials r-egg bioconductor-deseq2 multiprocess

   ..

    \*Windows users can install `deseq2 <https://bioconductor.org/packages/release/bioc/html/DESeq2.html>`__ in R via a cmd/anaconda prompt


   4. Install CRISPRO

   .. code:: bash

      wget https://gitlab.com/bauerlab/crispro/-/archive/main/crispro-main.zip
      unzip crispro-main.zip
      cd crispro-main
      python setup.py install

   ..

Usage
-----

There are two entry points into CRISPRO, fastq files and scores:

1. **Fastq Files**

   If starting with fastq files the following are required:

-  *--identifiers*, either a list of genes or ensembl
   (gene/transcript/peptided) ids entered at the command line or in a
   delimited file with one identifier per line
-  *--fastq*, fastq files containg sgRNAs from CRISPR saturating
   mutagenesis screen. All of the fastq files are entered in the command
   line where technical replicates are separated by ',', biological
   replicates are separated by ':' and samples are separated by ' '
   (spaces).
-  *--samples*, names for samples. Must be in same order as fastq
   samples
-  *--comparisons*, comma separated list of comparions (i.e.
   treatment1,control1 treatment2,control2)
-  *--annotation_file*, path to CRISPRO annotation file

   **Optional Arguments**

   -  *--positive-controls*, list of positive control guides in delimited
      file with one guide per line
   -  *--negative-controls*, list of negative (nontargeting) control
      guides to normalize functional scores to
   -  *--adapter*, sequence preceding sgRNA in fastq files (will be
      removed via cutadapt)
   -  *--scoring-method* {choose ALFC or DESEQ2}, Method of calculating scores.
      ALFC is the average log2 fold change of replicates. DeSeq2 option uses the
      DeSeq2 R package to calculate Log2 Fold change for guides. (default: ALFC)
   -  *--guide-len*, Length of guides (default: 20)

2. **Score file**

   If starting with scores the following are required:

-  *--scores*, delimited file. Two options:
    1. Column 1 contains identifiers (i.e. genename or Ensembl Peptide ID). Column 2 contains amino acid
       position. Column 3 and beyond contain scores for each amino acid.

    2. Column 1 contains sgRNA sequence. Column 2 and beyond contain scores for each of these guides.

-  *--identifiers*, either a list of genes or ensembl
   (gene/transcript/peptided) ids entered at the command line or in a
   delimited file with one identifier per line

-  *--annotation_file*, path to CRISPRO annotation file

Other *optional* arguments for CRISPRO are:

-  *--outdir*, Directory to output results/intermediate files.
-  *--num-processes*, Number of processes (to calculate scores from fastq
   files (default: 1))
-  *--loess-formula*, Annotations to use for loess regression modeling (i.e.
   position + provean_score) (default: position)
-  *--color-structure*, Download available structure files (pdb), align and
   recolor based on calculated scores (default: False)
-  *--keep-temp*, Keep intermediate output files: B-factor files as
   input for coloring structure and seperate alignment files
   PDB to complete protein (default: False)
-  *--principal-iso*, Define principle isoform of protein to map to, based
   on APPRIS annotations. If not given, will map to all available isoforms
   (default: False)
-  *--isoform-list*, Optional if *--principal-iso* is given: space separated
    list of prefered ensembl transcript IDs (ENSTID) of isoforms to map to
-  *--extra-pdb*, Comma separated list of pdb-chain and ensembl peptide
   id (i.e. PDB1-A,ENSP1 PDB2-B,ENSP2)
-  *--def-hits*, List of scores and per score list of hits (either gene
   names,ensembl peptide, transcript or gene ids). Separate score by space ' ',
   score and hits by ':', and individual hits by ','.
   (i.e. score1:ENSP1,ENSP2 score2:ENSP3) If no hits are given for a score,
   default hit definition is used.
-  *--offtarget-filter*, Off-target score filter, every guide with a lower off-
   target score will be removed (default: 5)


User example
------------


Run CRISPRO with `example input data <../example_data>`_ from CRISPR saturating mutagenesis on *BRAF* and *MAP2K1*, by `Donovan <http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0170445>`_ *et al.*

We will use the `annotation file <../annotations>`_ for GRCh37.

.. code:: bash

 crispro -i example_data/Donovan2017_IDs.txt \
        --scores example_data/Donovan2017PLoSONE_inputCRISPRO.csv \
        -o outdir_CRISPROexample \
        -a annotations/CRISPRO.GRCh37.ensembl90.20180901.csv.gz

..

Optional arguments might include:

.. code:: bash

  --num-processes 4 \
  --def-hits A375-selumetinib:BRAF,MAP2K1 A375-vemurafenib:BRAF,MAP2K1 MELJUSO-selumetinib:MAP2K1,BRAF \
  --color-structure \
  --extra-pdb 4MNE-A,ENSP00000302486 4MNE-D,ENSP00000302486 4MNE-E,ENSP00000302486 4MNE-H,ENSP00000302486 4MNE-B,ENSP00000288602 4MNE-C,ENSP00000288602 4MNE-F,ENSP00000288602 4MNE-G,ENSP00000288602

..

Output
------


-  score\_summary (folder)

   -  violinplots, showing the distribution of each score for each gene

   .. image:: ./example_data/CRISPROviolins.png
     :alt: CRISPRO violinplot hit example

   -  plots for every annotation compared to each score,
      pooled for negative and positive hits, and separately for every gene

   .. image:: ./example_data/CRISPROexamplePlot.png
      :alt: CRISPRO example plot annotations

   - csv showing general statistics for every score (per gene)

    *crispro\_stats\_score.csv*

   -  csv containing all outcomes of statistical tests for annotations, for every score
      (per gene, and pooled for negative and positive hits)

     *score\_annotation\_stats.csv*

-  linear\_tracks (folder)

   -  pdf containing primary sequence level (1D) visualization of data
      with domain, secondary structure, and other tracks laid below

      .. image:: ./example_data/CRISPROlineartrack.png
         :alt: CRISPRO example linear map

-  structures (folder)

   -  recolored pdbs by functional score saved as pymol session.
      Colorbars are found in *./annotations/*.

   .. image:: ./example_data/CRISPROstructure3DV3.png
      :name: CRISPRO example recolored structure, PDB ID: 3DV3

 PDB ID: `3DV3 <https://www.rcsb.org/structure/3DV3>`_, *MAP2K1*

-  colored\_protein\_sequences (folder)

   -  pdfs containing heatmap representation of primary sequence with 50
      amino acids per page. Colorbars are found in *./annotations/*.

   .. image:: ./example_data/CRISPROcoloredseq.png
     :alt: CRISPRO colored sequence example

- other files

  -  crispro\_overview\_guide\_density.csv, overview of the density of guides in the genes ran through CRISPRO.

  -  **crispro\_scores\_annotated-loess.csv**, complete overview of mapped all CRISPRO annotations, scores,
     and LOESS regression scores.

  -  crispro\_PDB\_alignments.csv, same complete overview of CRISPRO annotations, including all PDB alignments
     (when *--color-structure* was used).

  -  Distribution for every score, on which coloring bins are based. Colorbars are found in *./annotations/*.

How to cite CRISPRO
-------------------


If you use CRISPRO, please cite:

   Schoonenberg, Vivien A.C., Mitchel A. Cole, Qiuming Yao, Claudio Macias-Treviño, Falak Sher, Patrick G. Schupp, Matthew C. Canver, Takahiro Maeda, Luca Pinello, and Daniel E. Bauer. “CRISPRO: Identification of Functional Protein Coding Sequences Based on Genome Editing Dense Mutagenesis.” Genome Biology. 2018;19(1):169. https://doi.org/10.1186/s13059-018-1563-5
